#!/bin/bash

apt-get install python3 python3-pip virtualenv && virtualenv -p python3 ~/ansible && cd ~/ansible && source bin/activate && pip3 install ansible && bash

